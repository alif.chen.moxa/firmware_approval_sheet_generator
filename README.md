# Firmware_Approval_sheet_generator

## Usage: 
##### step1:

Prepare the pictures needed for Firmware Approval in `./pic` folder

`bootloader.png`, `checksum.png`, `ifconfig.png`, `mx-ver.png`

![prepare pictures](./ingredient/prepare_pic.png)

##### step2:

Download the and put it into approval folder, so the whole folder looks like:

![what should be in folder](./ingredient/what_should_in_folder.png)

##### step3:

Execute the tool and fulfill all information inside such as:

Please `close all Microsoft Word first`

![Please fulfill information](./ingredient/sample_tool.png)

##### step4:

Generating firmware approval docs

![generating](./ingredient/replacing.png)

After all process, the folder will look like:

![complete](./ingredient/complete_step_1.png)

##### step5:

Open the approval doc and fulfill other information needed:

![fulfill other](./ingredient/add_other_content.png)

Check all information is correct

##### step6:

Open `summery.txt`, check the ZIP file name. 

That is the final ZIP name should be.

![final](./ingredient/zip_file_name.png)

Please zip `image` and `approval doc` into the ZIP file

##### step7:

All the process is done. Please uploade and continue ...

