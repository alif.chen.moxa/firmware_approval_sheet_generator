# -*- coding: utf-8-*- 
import win32com
import os, time
from os import getcwd, listdir
from win32com.client import Dispatch, constants
import win32com.client.dynamic
import atexit
import code
import readline

def OpenWordApp():
    app  = win32com.client.Dispatch("Word.Application")
    app.Visible = True #Keep comment after tests
    app.DisplayAlerts = False
    return app
 
def MS_Word_Find_Replace(app, Search_Word, replace_str):
    wdStory = 6
    app.Selection.HomeKey(Unit=wdStory)
    find = app.Selection.Find
    find.Text = Search_Word
    while app.Selection.Find.Execute():
        app.Selection.TypeText(Text=replace_str)
        print("Find It ", Search_Word)
 
def MS_Wrod_SaveAS(app, fileName):
    print(fileName)
    app.ActiveDocument.SaveAs(fileName)
    app.ActiveDocument.Close()
    app.Quit() # releases Word object from memory
 
def pic_resize(pic, width) :
    ratio= pic.Height / pic.Width
    pic.Width = width
    pic.Height = ratio * pic.Width
    return

def main():
    file_old = open("summery.txt", mode="r", encoding = 'utf8')
    file_new = open("summery.csv", mode="w", encoding = 'utf8')
    for line in file_old.readlines():
        line_split = line.split(":",1)
        re_str = str(line_split[0]) + ":," + str(line_split[1].split(sep="\n")[0]).replace(",", "、")
        file_new.write(re_str + "\n")
    file_new.write(",,")
    file_old.close()
    file_new.close()
    #os.remove("summery.txt")

    FromTo = {"ModelName":"1AAA",
              "ProductNo":"2BBB",
              "ProductName":"3CCC",
              "Account/Password":"4DDD",
              "T2 Burn-in":"5EEE",
              "Builddate":"6FFF",
              "LANs":"7GGG",
              "ProjectNo":"8HHH",
              "ProjectName":"9III",
              "ProductModels":"10JJJ",
              "FormAuthor":"11KKK",
              "FWRver":"12LLL",
              "BIOSver":"13MMM",
              "Os Type":""} # 品號、品名、專案號碼、產品

    #Start Read from summery.csv
    File_summery = open("summery.csv", mode="r", encoding = 'utf8')
    for line in File_summery.readlines():
        if line.split(sep=",")[0] == "Model Name:" :
            FromTo["ModelName"] = line.split(sep=",")[1].split(sep="\n")[0]
        if line.split(sep=",")[0] == "料號:" :
            FromTo["ProductNo"] = line.split(sep=",")[1].split(sep="\n")[0]
        elif line.split(sep=",")[0] == "品名:" :
            FromTo["ProductName"] = line.split(sep=",")[1].split(sep="\n")[0]
        elif line.split(sep=",")[0] == "Account/Password:" :
            FromTo["Account/Password"] = line.split(sep=",")[1].split(sep="\n")[0]
        elif line.split(sep=",")[0] == "T2 Burn-in:" :
            FromTo["T2 Burn-in"] = line.split(sep=",")[1].split(sep="\n")[0]
        elif line.split(sep=",")[0] == "Builddate:" :
            FromTo["Builddate"]  = line.split(sep=",")[1].split(sep="\n")[0]
            BD_2 = FromTo["Builddate"][2:4] + "-" + FromTo["Builddate"][4:6] + "-" + FromTo["Builddate"][0:2]
        elif line.split(sep=",")[0] == "Number of LANs:" :
            FromTo["LANs"] = int(line.split(sep=",")[1])
        elif line.split(sep=",")[0] == "專案號碼:" :
            FromTo["ProjectNo"] = line.split(sep=",")[1].split(sep="\n")[0] # update existing entry
        elif line.split(sep=",")[0] == "專案名稱:" :
            FromTo["ProjectName"] = line.split(sep=",")[1].split(sep="\n")[0] # update existing entry
        elif line.split(",",1)[0] == "適用型號:" :
            FromTo["ProductModels"] = line.split(",",1)[1].split(sep="\n")[0]
        elif line.split(",",1)[0] == "承認書撰寫人:" :
            FromTo["FormAuthor"] = line.split(",",1)[1].split(sep="\n")[0]
        elif line.split(",",1)[0] == "Firmware Version:" :
            FromTo["FWRver"] = line.split(",",1)[1].split(sep="\n")[0]
        elif line.split(",",1)[0] == "BIOS Version:" :
            FromTo["BIOSver"] = line.split(",",1)[1].split(sep="\n")[0]
        elif line.split(sep=",")[0] == "Os Type:" :
            FromTo["Os Type"] = line.split(sep=",")[1].split(sep="\n")[0]
    File_summery.close()

    # is LX or not
    NetSetting = ""
    print(FromTo["Os Type"])
    if "LX" in FromTo["Os Type"] :
        pic_path = ["\\checksum.png", "\\mx-ver.png", "\\ifconfig.png"]
        for LAN in range(0, FromTo["LANs"]) :
            NetSetting = NetSetting+"\n\t\tLAN" + str(LAN+1) + ":192.168." + str(LAN+3) + ".127"
    else:
        pic_path = ["\\checksum.png", "\\mxver.png", "\\ipconfig.png"]
        for LAN in range(0, FromTo["LANs"]) :
            NetSetting = NetSetting + "\n\t\tLAN" + str(LAN+1) + ":DHCP"

    #docs = [i for i in listdir('.') if i[-3:]=='doc' or i[-4:]=='docx'] #All Word file
    #MSword = ReadWrod('{}\\{}'.format(getcwd(), "PD.156_v1.7.doc"))

    app  = OpenWordApp()
    MSword = app.Documents.Open('{}\\{}'.format(getcwd(), "HQ-PD.156_v1.8_程式類承認書_modified.doc"))

    # 取代預設於 word 內的文字
    for From in FromTo.keys():
        MS_Word_Find_Replace(app, From, FromTo[From])

    # input date   
    DateToday = time.strftime("%m-%d-%Y", time.localtime())
    DateToday = DateToday[0:6]+DateToday[8:10]
    MSword.Tables(3).Cell(2, 5).Range.InsertBefore(DateToday)
    MSword.Tables(4).Cell(8, 8).Range.InsertBefore(BD_2)

    # input approval zip file
    MSword.Tables(4).Cell(8, 4).Range.InsertBefore("{}_{}_build_{}.zip".format(FromTo["ProductNo"], FromTo["ProductName"], FromTo["Builddate"]))

    # input firmware and bootloader version
    MSword.Tables(4).Cell(8, 6).Range.InsertBefore("FWR: {}\nBIOS: {}".format(FromTo["FWRver"], FromTo["BIOSver"]))
    
    #input checksum picture.
    pic_0 = MSword.Tables(4).Cell(6, 3).Range.InlineShapes.AddPicture(os.getcwd()+ "\\pic" + pic_path[0])
    pic_resize(pic_0, 400)
    MSword.Tables(4).Cell(6, 3).Range.InsertBefore("\n\n\n\n\n" + "Checksum :\n")

    #input other informaton
    MSword.Tables(4).Cell(15, 2).Range.InsertBefore("\n6.\tT2 Burn-in:" + FromTo["T2 Burn-in"])
    pic_2 = MSword.Tables(4).Cell(15, 2).Range.InlineShapes.AddPicture(os.getcwd()+ "\\pic" + pic_path[2])
    pic_resize(pic_2, 400)
    """ Take off NetSetting
    MSword.Tables(4).Cell(15, 2).Range.InsertBefore("\n4.\tCheck MAC order: Done" +\
                                                  "\n5.\tNetwork settings:" + NetSetting +"\n\t")
    """
    MSword.Tables(4).Cell(15, 2).Range.InsertBefore("\n4.\tCheck MAC order: Done" +\
                                                  "\n5.\tNetwork settings:" +"\n\t")
    pic_1 = MSword.Tables(4).Cell(15, 2).Range.InlineShapes.AddPicture(os.getcwd()+ "\\pic" + pic_path[1])
    pic_resize(pic_1, 300)
    MSword.Tables(4).Cell(15, 2).Range.InsertBefore("1.\t使用" + FromTo["ModelName"] + "產品進行驗證，測試結果為PASS。" +\
                                                  "\n2.\tAccount/Password : " + FromTo["Account/Password"] +\
                                                  "\n3.\tBuild date of \'kversion\' (if ThingsPro : \'pversion\') :\n\t")
    #ext = doc.rsplit('.',1)[1]
    MS_Wrod_SaveAS(app, ("{}\\{}_{}_承認書.doc".format(getcwd(), FromTo["ProductNo"], FromTo["ProductName"])))

if __name__ == '__main__':
    main()
