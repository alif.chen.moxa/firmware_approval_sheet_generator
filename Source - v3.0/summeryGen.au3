#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <FileConstants.au3>
#include <MsgBoxConstants.au3>
#include <WinAPIFiles.au3>
#include <ComboConstants.au3>

#Region ### START Koda GUI section ### Form=d:\alif_chen\desktop\2. approval_pom_package\1. sample&tool\approval_gen\sourcecode - v2.1\approvalgen.kxf
$Form1_1 = GUICreate("Rom/Image Approval Tool", 600, 730, 535, 44)
$Label_A01 = GUICtrlCreateLabel("How to do firmware approval", 30, 8, 494, 31)
GUICtrlSetFont(-1, 14, 400, 4, "Arial monospaced for SAP")
GUICtrlSetColor(-1, 0x800000)
$Label_A02 = GUICtrlCreateLabel("You need the following screen prints in ./pic", 10, 50, 454, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_A03 = GUICtrlCreateLabel("1. mx-ver.png  /  mxver.png", 10, 70, 294, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_A04 = GUICtrlCreateLabel("2. ifconfig.png  /  ipconfig.png", 10, 90, 324, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_A05 = GUICtrlCreateLabel("3. checksum.png", 10, 110, 154, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_A06 = GUICtrlCreateLabel("4. bootloader.png  /  F11.bmp", 10, 130, 294, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")

$Label_B01 = GUICtrlCreateLabel("Model Name:", 10, 176, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B02 = GUICtrlCreateLabel("料號:", 10, 216, 54, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B03 = GUICtrlCreateLabel("品名:", 10, 256, 54, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B04 = GUICtrlCreateLabel("登入帳號/密碼:", 10, 296, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B05 = GUICtrlCreateLabel("T2 BurnIn 方式:", 10, 336, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B06 = GUICtrlCreateLabel("Build_Date:", 10, 376, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B07 = GUICtrlCreateLabel("Number of LANs:", 10, 416, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B08 = GUICtrlCreateLabel("專案號碼:", 10, 456, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B09 = GUICtrlCreateLabel("專案名稱:", 10, 496, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B10 = GUICtrlCreateLabel("適用型號:", 10, 536, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B11 = GUICtrlCreateLabel("承認書撰寫人:", 10, 576, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B12 = GUICtrlCreateLabel("Release Note:", 10, 616, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B13 = GUICtrlCreateLabel("Firmware Version:", 10, 656, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Label_B14 = GUICtrlCreateLabel("BIOS Version:", 10, 696, 160, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")

$Input_B01 = GUICtrlCreateInput("", 172, 170, 177, 24)
$Group1 = GUICtrlCreateGroup("OS type", 370, 152, 217, 57)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$Radio1 = GUICtrlCreateRadio("LX", 380, 170, 57, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Radio2 = GUICtrlCreateRadio("Win", 440, 170, 57, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Radio3 = GUICtrlCreateRadio("Other", 510, 170, 73, 24)
GUICtrlSetFont(-1, 10, 400, 0, "Arial monospaced for SAP")
$Input_B02 = GUICtrlCreateInput("", 172, 210, 177, 24)
$Input_B03 = GUICtrlCreateInput("", 172, 250, 177, 24)
$Combo1 = GUICtrlCreateCombo("N/A", 172, 290, 177, 24, BitOR($CBS_DROPDOWN,$CBS_AUTOHSCROLL))
$Combo2 = GUICtrlCreateCombo("N/A", 172, 330, 177, 25, BitOR($CBS_DROPDOWN,$CBS_AUTOHSCROLL))
$Input_B06 = GUICtrlCreateInput("", 172, 370, 177, 24)
$Input_B07 = GUICtrlCreateInput("", 172, 410, 177, 24)
$Input_B08 = GUICtrlCreateInput("", 172, 450, 177, 24)
$Input_B09 = GUICtrlCreateInput("", 172, 490, 420, 24)
$Input_B10 = GUICtrlCreateInput("", 172, 530, 420, 24)
$Input_B11 = GUICtrlCreateInput("", 172, 570, 177, 24)
$Checkbox_1 = GUICtrlCreateCheckbox ("Release Note prepared", 172, 610)
$Input_B13 = GUICtrlCreateInput("", 172, 650, 177, 24)
$Input_B14 = GUICtrlCreateInput("", 172, 690, 177, 24)

GUICtrlSetData(-1, "")
$Button1 = GUICtrlCreateButton("Create", 400, 320, 99, 65)
GUICtrlSetFont(-1, 12, 400, 0, "Ribbon")
GUICtrlSetColor(-1, 0x0000FF)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

; Add additional items to the combobox.
GUICtrlSetData($Combo1, "moxa/moxa", "N/A")
GUICtrlSetData($Combo2, "DS-Apburn", "N/A")

Global $OS_Type = "Empty"
While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		Case $Button1
			if ($OS_Type == "Empty") Then
				MsgBox($MB_ICONWARNING, "Please choose OS type", "Please choose OS Type")
				ContinueLoop
			ElseIf Not _IsChecked($Checkbox_1) Then
				MsgBox($MB_ICONWARNING, "Please check Release Note", "Please check Release Note")
				ContinueLoop
			EndIf
			summeryGen()
			Exit
		Case $Radio1 ;And BitAND(GUICtrlRead($Radio1), $GUI_CHECKED) = $GUI_CHECKED
			$OS_Type = "LX"
		Case $Radio2 ;And BitAND(GUICtrlRead($Radio2), $GUI_CHECKED) = $GUI_CHECKED
			$OS_Type = "Win"
		Case $Radio3 ;And BitAND(GUICtrlRead($Radio3), $GUI_CHECKED) = $GUI_CHECKED
			$OS_Type = "Other OS"
	EndSwitch
WEnd

Func _IsChecked($idControlID)
    Return BitAND(GUICtrlRead($idControlID), $GUI_CHECKED) = $GUI_CHECKED
EndFunc   ;==>_IsChecked

Func summeryGen()
    ; Create a constant variable in Local scope of the filepath that will be read/written to.
    ;Local Const $sFilePath = _WinAPI_GetTempFileName("D:\Alif_Chen\Desktop\2. Approval_POM_package\1. Sample&tool\Approval_Gen\test.csv")
	Local const $sFilePath = FileOpen(".\summery.txt", $FO_OVERWRITE)

    ; Open the file for writing (append to the end of a file) and store the handle to a variable.
    ;Local $hFileOpen = FileOpen($sFilePath, $FO_APPEND)
    If $sFilePath = -1 Then
        MsgBox($MB_SYSTEMMODAL, "", "An error occurred whilst writing the temporary file.")
        Return False
    EndIf

	; Write data to the file using the handle returned by FileOpen.
	Dim $Txt[14]
	$Txt[0] = GUICtrlRead($Input_B01) ; model name
	$Txt[1] = GUICtrlRead($Input_B02) ; 料號
	$Txt[2] = GUICtrlRead($Input_B03) ; 品名
	$Txt[3] = GUICtrlRead($Combo1) ; account
	$Txt[4] = GUICtrlRead($Combo2) ; T2
	$Txt[5] = GUICtrlRead($Input_B06) ; builddate
	$Txt[6] = GUICtrlRead($Input_B07) ; # lans
	$Txt[7] = GUICtrlRead($Input_B08) ; 專案號碼
	$Txt[8] = GUICtrlRead($Input_B09) ; 專案名稱
	$Txt[9] = GUICtrlRead($Input_B10) ; 適用產品
	$Txt[10] = $OS_Type
	$Txt[11] = GUICtrlRead($Input_B11) ; 承認書撰寫人
	$Txt[12] = GUICtrlRead($Input_B13) ; firmware version
	$Txt[13] = GUICtrlRead($Input_B14) ; bootloader version

	FileWriteLine($sFilePath, "Model Name:" & $Txt[0] & @CRLF)
	FileWriteLine($sFilePath, "料號:" & $Txt[1] & @CRLF)
	FileWriteLine($sFilePath, "品名:" & $Txt[2] & @CRLF)
	FileWriteLine($sFilePath, "Account/Password:" & $Txt[3] & @CRLF)
	FileWriteLine($sFilePath, "T2 Burn-in:" & $Txt[4] & @CRLF)
	FileWriteLine($sFilePath, "Builddate:" & $Txt[5] & @CRLF)
	FileWriteLine($sFilePath, "Number of LANs:" & $Txt[6] & @CRLF)
	FileWriteLine($sFilePath, "專案號碼:" & $Txt[7] & @CRLF)
	FileWriteLine($sFilePath, "專案名稱:" & $Txt[8] & @CRLF)
	FileWriteLine($sFilePath, "適用型號:" & $Txt[9] & @CRLF)
	FileWriteLine($sFilePath, "承認書撰寫人:" & $Txt[11] & @CRLF)
	FileWriteLine($sFilePath, "Firmware Version:" & $Txt[12] & @CRLF)
	FileWriteLine($sFilePath, "BIOS Version:" & $Txt[13] & @CRLF)
	FileWriteLine($sFilePath, "Os Type:" & $Txt[10] & @CRLF)
	FileWriteLine($sFilePath, "Approval Zip Name:" & $Txt[1] & "_" & $Txt[2] & "_build_" & $Txt[5] & ".zip" & @CRLF)
	FileWriteLine($sFilePath, "Approval Doc Name:" & $Txt[1] & "_" & $Txt[2] & "_承認書.doc" & @CRLF)
    ; Close the handle returned by FileOpen.
    FileClose($sFilePath)

    ; Display the contents of the file passing the filepath to FileRead instead of a handle returned by FileOpen.
	$sFileCheck = FileOpen(".\summery.txt", $FO_READ)
	;MsgBox($MB_SYSTEMMODAL, "Mission Complete !!! ", FileRead($sFileCheck))
	MsgBox($MB_ICONINFORMATION, "Approval initial Complete !!! ", "Step 1 : Done" & @CRLF & "Step 2 : fulfill NPDP/ECN/CV、修訂內容、改版說明")
	FileClose($sFileCheck)
EndFunc   ;==>summeryGen
